defmodule Webapp.BattleControllerTest do
  use Webapp.ConnCase

  alias Webapp.Battle
  @valid_attrs %{link: "some content", name: "some content", round: 42, status: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, battle_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    battle = Repo.insert! %Battle{}
    conn = get conn, battle_path(conn, :show, battle)
    assert json_response(conn, 200)["data"] == %{"id" => battle.id,
      "status" => battle.status,
      "user_id" => battle.user_id,
      "round" => battle.round,
      "link" => battle.link,
      "name" => battle.name}
  end

  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, battle_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, battle_path(conn, :create), battle: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Battle, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, battle_path(conn, :create), battle: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    battle = Repo.insert! %Battle{}
    conn = put conn, battle_path(conn, :update, battle), battle: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Battle, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    battle = Repo.insert! %Battle{}
    conn = put conn, battle_path(conn, :update, battle), battle: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    battle = Repo.insert! %Battle{}
    conn = delete conn, battle_path(conn, :delete, battle)
    assert response(conn, 204)
    refute Repo.get(Battle, battle.id)
  end
end
