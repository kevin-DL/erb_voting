defmodule Webapp.UserTest do
  use Webapp.ModelCase

  alias Webapp.User

  @valid_attrs %{admin: true, email: "some content", image: "some content", nickname: "some content", provider: "some content", status: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
