defmodule Webapp.BattleTest do
  use Webapp.ModelCase

  alias Webapp.Battle

  @valid_attrs %{link: "some content", name: "some content", round: 42, status: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Battle.changeset(%Battle{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Battle.changeset(%Battle{}, @invalid_attrs)
    refute changeset.valid?
  end
end
