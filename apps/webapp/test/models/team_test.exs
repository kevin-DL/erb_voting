defmodule Webapp.TeamTest do
  use Webapp.ModelCase

  alias Webapp.Team

  @valid_attrs %{name: "some content", normalized_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Team.changeset(%Team{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Team.changeset(%Team{}, @invalid_attrs)
    refute changeset.valid?
  end
end
