defmodule Webapp.ContestantTest do
  use Webapp.ModelCase

  alias Webapp.Contestant

  @valid_attrs %{name: "some content", normalized_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Contestant.changeset(%Contestant{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Contestant.changeset(%Contestant{}, @invalid_attrs)
    refute changeset.valid?
  end
end
