/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	// main.js
	//services

	// require a *.vue component
	var Home = __webpack_require__(1)
	var Login = __webpack_require__(16)
	var About =  __webpack_require__(21)

	// Contestants
	var ContestantHome = __webpack_require__(26)
	var ContestantNew = __webpack_require__(30)
	var ContestantIndex = __webpack_require__(38)
	var ContestantShow = __webpack_require__(41)

	// Battles
	var BattleHome = __webpack_require__(46)
	var BattleNew = __webpack_require__(50)
	var BattleIndex = __webpack_require__(55)
	var BattleShow = __webpack_require__(61)

	// Teams
	var TeamHome = __webpack_require__(66)
	var TeamNew = __webpack_require__(70)
	var TeamIndex = __webpack_require__(75)
	var TeamShow = __webpack_require__(81)



	// The router needs a root component to render.
	// For demo purposes, we will just use an empty one
	// because we are using the HTML as the app template.
	// !! Note that the App is not a Vue instance.

	var App = Vue.extend({
	})

	// Create a router instance.
	// You can pass in additional options here, but let's
	// keep it simple for now.
	var router = new VueRouter({
	    history: false,
	    linkActiveClass: 'disabled'
	})

	// Define some routes.
	// Each route should map to a component. The "component" can
	// either be an actual component constructor created via
	// Vue.extend(), or just a component options object.
	// We'll talk about nested routes later.
	router.map({
	    '/': {
	      component: Home
	    },
	    '/login': {
	        component: Login
	    },
	    '/about': {
	        component: About
	    },
	    '/contestants':{
	        component: ContestantHome,

	        subRoutes:{
	            '/': {
	                component: ContestantIndex,
	            },
	            '/new':{
	                component: ContestantNew
	            },
	            '/:id':{
	                name: 'showContestant',
	                component: ContestantShow
	            }

	        }
	    },
	    '/battles':{
	        component: BattleHome,

	        subRoutes:{
	            '/': {
	                component: BattleIndex
	            },
	            '/new':{
	                component: BattleNew
	            },
	            '/:id':{
	                name: 'showBattle',
	                component: BattleShow
	            }

	        }
	    },
	    '/teams':{
	        component: TeamHome,
	        subRoutes:{
	            '/':{
	                component: TeamIndex
	            },
	            '/new':{
	                component: TeamNew
	            },
	            '/:id':{
	                name: 'showTeam',
	                component: TeamShow
	            }
	        }
	    }
	})

	router.beforeEach(function () {
	    window.scrollTo(0, 0)
	})

	// Now we can start the app!
	// The router will create an instance of App and mount to
	// the element matching the selector #app.
	router.start(App, '#app')


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(2)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/home.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(15)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/home.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var Top = __webpack_require__(3);
	var Bottom = __webpack_require__(10);

	exports.default = {
	    components: {
	        Top: Top,
	        Bottom: Bottom
	    }
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(4)
	__vue_script__ = __webpack_require__(8)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/top.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(9)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/top.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(5);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./top.vue", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./top.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 6 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];

		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};

		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();

		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}

	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}

	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	var replaceText = (function () {
		var textStore = [];

		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if (media) {
			styleElement.setAttribute("media", media);
		}

		if (sourceMap) {
			// https://developer.chrome.com/devtools/docs/javascript-debugging
			// this makes source maps inside style tags work properly in Chrome
			css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */';
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['subject', 'list', 'total'],

	    created: function created() {
	        this.fetchTop();
	    },
	    methods: {
	        fetchTop: function fetchTop() {
	            this.$http.get('/api/top/' + this.subject).then(function (data) {
	                var received_data = data.data;
	                this.list = received_data.data;
	                this.total = received_data.total;
	            }, function (error) {
	                console.log(error);
	            });
	        }
	    }
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = "\n<div>\n    <h3>Top Ten {{ subject | capitalize }} of {{ total }}</h3>\n    <ul class=\"list-group\">\n        <li class=\"list-group-item\" v-for=\"item in list\" :item=\"item\" :index=\"$index\">\n            <span> {{ $index + 1 }} </span> {{item.name}}<span v-show=\"item.nb_votes\" class=\"glyphicon glyphicon-star\" aria-hidden=\"true\">({{item.nb_votes}})</span>\n        </li>\n    </ul>\n</div>\n";

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(11)
	__vue_script__ = __webpack_require__(13)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/bottom.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(14)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/bottom.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(12);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./bottom.vue", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./bottom.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 13 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['subject', 'list', 'total']
	};

/***/ },
/* 14 */
/***/ function(module, exports) {

	module.exports = "\n<div>\n    <h3>Bottom Ten {{subject | capitalize }}</h3>\n</div>\n<ul class=\"list-group\">\n    <li class=\"list-group-item\" v-for=\"item in list\" :item=\"item\" :index=\"$index\">\n        <span> {{ $index + 1 }} </span> {{item.name}}\n    </li>\n</ul>\n";

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"jumbotron\">\n    <h2>Welcome to ERB Voting</h2>\n    <p> This is an unofficial app, not supported in anyway by ERB. </p>\n    <p class=\"lead center-text\" > Here you will be able to submit and vote for the battles you want to see acted by Nice Peter and Epic LLoyd!  </p>\n</div>\n<div class=\"container-fluid\">\n    <top class=\"col-lg-4 text-center\" subject=\"contestants\"></top>\n    <top class=\"col-lg-4 text-center\" subject=\"teams\"></top>\n    <top class=\"col-lg-4 text-center\" subject=\"battles\"></top>\n</div>\n";

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(17)
	__vue_script__ = __webpack_require__(19)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/login.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(20)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/login.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(18);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./login.vue", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./login.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 19 */
/***/ function(module, exports) {

	"use strict";

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"jumbotron\">\n    <a class=\"btn btn-primary btn-lg\" href=\"/auth/google\">\n        <i class=\"fa fa-google\"></i>\n        Sign in with Google\n    </a>\n</div>\n";

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(22)
	__vue_script__ = __webpack_require__(24)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/about.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(25)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/about.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(23);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./about.vue", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./about.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 24 */
/***/ function(module, exports) {

	"use strict";

/***/ },
/* 25 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"alert alert-danger\">\n  This is a Beta version, i am only making it to learn new technologies.\n</div>\n<div class=\"center\">\n    <h4 class=\"text-center center\"> What is ERB Voting? </h4>\n\n    <p> ERB Voting is a simple project that i created in order to learn <a href=\"http://phoenixframework.com\"> Phoenix</a>\n        and <a href=\"http://elixir-lang.org\"> Elixir </a>.<br>\n\n        It allows connected users to view, search, suggest and vote for future Epic Rap Battles of History.<br>\n        The users need to be logged in using on of the possible identity providers:\n    <ul>\n        <li> Google </li>\n    </ul>\n\n    One user can only vote once for a battle, there is currently only the possibility to upvote.<br>\n    One can suggest as many battles a one wants however, please search for a battle before submitting it. <br>\n    Someone might already have done so. <br>\n    </p>\n</div>\n";

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(27)
	__vue_template__ = __webpack_require__(29)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/contestants/home.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(28);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 29 */
/***/ function(module, exports) {

	module.exports = "\n<div>\n    <header>\n        <ol class=\"breadcrumb\">\n            <li v-link=\"{ path:  '/contestants' , append: true}\" class=\"btn\">The List</li>\n            <li v-link=\"{ path: '/contestants/new', append: true }\"  class=\"btn\">Create a new Contestant</li>\n        </ol>\n    </header>\n    <div class=\"container-fluid\">\n        <router-view\n                keep-alive>\n        </router-view>\n    </div>\n</div>\n";

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(31)
	__vue_script__ = __webpack_require__(33)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/contestants/new.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(37)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/contestants/new.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(32);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n\n", ""]);

	// exports


/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var Search = __webpack_require__(34);

	exports.default = {
	    props: ['parentSearch'],
	    components: {
	        Search: Search
	    },
	    methods: {
	        search_con: function search_con() {
	            this.$broadcast('search', this.parentSearch);
	        },
	        create_contestant: function create_contestant() {
	            this.$http.post('/api/contestants/create', { api_token: window.apiToken, contestant: { name: this.parentSearch } }).then(function (data) {
	                console.log(data);
	                if (data.data.msg) {
	                    alertify.success(data.data.msg);
	                    this.search_con();
	                    this.parentSearch = '';
	                }
	            }, function (error) {
	                console.log(error);
	                if (error.data.msg) {
	                    alertify.error(error.data.msg);
	                } else {
	                    alertify.error(error.data.errors);
	                }
	            });
	        }
	    }
	};

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(35)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/contestants/search.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(36)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/contestants/search.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 35 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['list', 'total', 'searchTerm', 'page', 'pages', 'show'],

	    created: function created() {
	        this.searchTerm = '';
	        this.page = 1;
	        this.pages = 1;
	        this.searching = false;
	        this.initialSearch(1);
	    },
	    methods: {
	        search: function search(page) {
	            if (this.searchTerm.replace(/\s+/g, '').length >= 3) {
	                this.searching = true;
	                this.$http.post('/api/search/contestants', { term: this.searchTerm, page: page }).then(function (data) {
	                    var received_data = data.data;
	                    this.list = received_data.data;
	                    this.total = received_data.total;
	                    this.page = page;
	                    this.pages = received_data.pages;
	                }, function (error) {
	                    console.log(error);
	                });
	            } else {
	                this.searching = false;
	                this.initialSearch(page);
	            }
	        },

	        initialSearch: function initialSearch(page) {
	            this.$http.get('/api/contestants/page/' + page).then(function (data) {
	                var received_data = data.data;
	                this.list = received_data.data;
	                this.total = received_data.total;
	                if (this.total <= 10) {
	                    this.page = 1;
	                } else {
	                    this.page = page;
	                }
	                this.pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        }
	    },
	    events: {
	        'search': function search(term) {
	            this.searchTerm = term;this.search(1);
	        }
	    }
	};

/***/ },
/* 36 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"container-fluid\">\n    <form class=\"form-horizontal\" v-show=\"show == 'true'\">\n        <div class=\"form-group text-center\">\n            <label for=\"inputContestant\">Filter</label>\n            <div>\n                <input type=\"text\" @keyup=\"search(1)\" v-model=\"searchTerm\" class=\"form-control\" id=\"inputContestant\" placeholder=\"Contestant, minimum 3 characters spaces not included\">\n            </div>\n        </div>\n    </form>\n    <table class=\"table table-striped table-bordered\" v-show=\"total\">\n        <caption> Contestants ({{total}}) </caption>\n        <thead>\n        <tr>\n            <th>\n                Number\n            </th>\n            <th>\n                Name\n            </th>\n            <th>\n                Number of teams\n            </th>\n            <th>\n                Number of battles\n            </th>\n            <th>\n                Number of votes\n            </th>\n        </tr>\n        </thead>\n        <tbody>\n        <tr v-for=\"item in list\" :item=\"item\" :index=\"$index\">\n            <td>\n                {{ ($index + 1)  + ((page - 1) * 10) }}\n            </td>\n            <td>\n                {{item.name}}\n            </td>\n            <td>\n                {{item.nb_teams}}\n            </td>\n            <td>\n                {{item.nb_battles}}\n            </td>\n            <td>\n                {{item.nb_votes}}\n            </td>\n        </tr>\n        </tbody>\n    </table>\n    <h3 v-else>No result found!</h3>\n    <div class=\"text-center\" v-show=\"total\">\n        <nav>\n            <ul class=\"pager\">\n                <li v-bind:class=\"{ 'disabled': page == 1}\" v-on:click=\"search(page - 1)\" class=\"btn\"><a>Previous</a></li>\n                <li>\n                    <strong> {{ page }}  / {{ pages }}</strong>\n                </li>\n                <li v-bind:class=\"{ 'disabled': page == pages}\" @click=\"search(page + 1)\" class=\"btn\"><a>Next</a></li>\n            </ul>\n        </nav>\n    </div>\n</div>\n";

/***/ },
/* 37 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"text-center\">\n    <h3> Create a New Contestant </h3>\n    <div class=\"container-fluid\" >\n        <div class=\"col-lg-3\">\n            <form class=\"form-horizontal\">\n                <div class=\"form-group text-center\">\n                    <label for=\"inputContestantAdd\">Search</label>\n                    <div>\n                        <input type=\"text\" @keyup=\"search_con\" v-model=\"parentSearch\" class=\"form-control\" id=\"inputContestantAdd\" placeholder=\"New Contestant Name\">\n                    </div>\n                </div>\n            </form>\n            <button @click=\"create_contestant\"> Add Contestant </button>\n        </div>\n        <div class=\"col-lg-7 col-lg-offset-1\">\n            <search show=\"false\" subject=\"contestants\"></search>\n        </div>\n    </div>\n</div>\n";

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(39)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/contestants/index.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(40)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/contestants/index.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var Search = __webpack_require__(34);

	exports.default = {
	    components: {
	        Search: Search
	    }
	};

/***/ },
/* 40 */
/***/ function(module, exports) {

	module.exports = "\n<search show=\"true\" subject=\"contestants\"></search>\n";

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(42)
	__vue_script__ = __webpack_require__(44)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/contestants/show.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(45)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/contestants/show.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(43);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 44 */
/***/ function(module, exports) {

	"use strict";

/***/ },
/* 45 */
/***/ function(module, exports) {

	module.exports = "\n\n";

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(47)
	__vue_template__ = __webpack_require__(49)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/battles/home.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(48);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 49 */
/***/ function(module, exports) {

	module.exports = "\n<div>\n    <header>\n        <ol class=\"breadcrumb\">\n            <li v-link=\"{ path:  '/battles' , append: true}\" class=\"btn\">The List</li>\n            <li v-link=\"{ path: '/battles/new', append: true }\"  class=\"btn\">Create a new  Battle</li>\n        </ol>\n    </header>\n    <div class=\"container-fluid\">\n        <router-view\n                keep-alive>\n        </router-view>\n    </div>\n</div>\n";

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(51)
	__vue_script__ = __webpack_require__(53)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/battles/new.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(54)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/battles/new.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(52);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n\n", ""]);

	// exports


/***/ },
/* 53 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['participantSearch', 'participants', 'teams', 'contestants', 'team_total', 'team_page', 'team_pages', 'contestant_total', 'contestant_page', 'contestant_pages'],
	    components: {},
	    created: function created() {
	        this.participantSearch = '';
	        this.page = 1;
	        this.pages = 1;
	        this.search_contestants(1);
	        this.participants = new Array();
	    },

	    computed: {
	        nb_participants: function nb_participants() {
	            return this.participants.length;
	        }
	    },
	    methods: {
	        search: function search() {
	            this.search_contestants(1);
	            this.search_teams(1);
	        },
	        addParticipant: function addParticipant(participant, type) {
	            participant.type = type;
	            var current_participant = participant;
	            if (this.nb_participants >= 5) {
	                alertify.error("The number of participants is limited to five!");
	                return true;
	            }
	            if (this.nb_participants > 0) {
	                var isNotCurrentContestant = function isNotCurrentContestant(element, index, array) {
	                    if (element.type == current_participant.type) {
	                        return current_participant.id != element.id;
	                    } else {
	                        return true;
	                    }
	                };

	                if (this.participants.every(isNotCurrentContestant)) {
	                    this.participants.push(current_participant);
	                } else {
	                    alertify.notify("Participant already selected for battle");
	                }
	            } else {
	                this.participants.push(participant);
	            }
	        },
	        deleteParticipant: function deleteParticipant(participant) {
	            this.participants.$remove(participant);
	        },
	        search_teams: function search_teams(page) {
	            this.$http.post('/api/search/teams', { term: this.participantSearch, page: page }).then(function (data) {
	                var received_data = data.data;
	                this.teams = received_data.data;
	                this.team_total = received_data.total;
	                this.team_page = page;
	                this.team_pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        },
	        search_contestants: function search_contestants(page) {
	            this.$http.post('/api/search/contestants', { term: this.participantSearch, page: page }).then(function (data) {
	                var received_data = data.data;
	                this.contestants = received_data.data;
	                this.contestant_total = received_data.total;
	                this.contestant_page = page;
	                this.contestant_pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        },
	        exists: function exists() {
	            if (this.nb_participants < 2) {
	                alertify.error("You need at least 2 participants to create a Battle");
	                return;
	            }

	            this.$http.post('/api/battles/exists', { api_token: window.apiToken, battle: { participants: this.participants } }).then(function (data) {
	                var received_data = data.data;
	                console.log(received_data);
	                if (received_data.exists == true) {
	                    alertify.confirm("This Battle already exists: " + data.data.name + ". Round: " + data.data.round + "<br/> Do you want to create a new round?", this.create_battle, this.reset);
	                } else {
	                    this.create_battle();
	                }
	            }, function (error) {
	                console.log(error);
	                if (error.data.msg) {
	                    alertify.error(error.data.msg);
	                } else {
	                    alertify.error(error.data.errors);
	                }
	            });
	        },
	        create_battle: function create_battle() {
	            this.$http.post('/api/battles/create', { api_token: window.apiToken, battle: { participants: this.participants } }).then(function (data) {
	                console.log(data);
	                if (data.data.msg) {
	                    alertify.success(data.data.msg);
	                    this.reset();
	                }
	            }, function (error) {
	                console.log(error);
	                if (error.data.msg) {
	                    alertify.error(error.data.msg);
	                } else {
	                    alertify.error(error.data.errors);
	                }
	                this.reset();
	            });
	        },
	        reset: function reset() {
	            this.participantSearch = "";
	            this.search();
	            this.participants = new Array();
	        }
	    }
	};

/***/ },
/* 54 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"text-center\">\n    <h3> Create a New Battle (Max 5 participants) </h3>\n    <div class=\"container-fluid\" >\n        <div class=\"col-lg-8\">\n            <form class=\"form-horizontal\" @submit=\"search\">\n                <div class=\"form-group text-center\">\n                    <label for=\"inputParticipantSearch\">Search</label>\n                    <div>\n                        <input type=\"text\" @keyup=\"search\" v-model=\"participantSearch\" class=\"form-control\" id=\"inputParticipantSearch\" placeholder=\"Search for potential participants\">\n                    </div>\n                </div>\n            </form>\n            <div v-show=\"teams\" class=\"row col-lg-6\">\n                <h4> Teams ({{ team_total }}) </h4>\n                <table class=\"table table-striped table-bordered\">\n                    <thead>\n                        <tr>\n                            <th>\n                                Name\n                            </th>\n                            <th>\n                                Members\n                            </th>\n                            <th>\n                                Add to Battle\n                            </th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr v-for=\"team in teams\">\n                            <td>\n                              {{team.name}}\n                            </td>\n                            <td>\n                                {{team.members}}\n                            </td>\n                            <td>\n                                <button @click=\"addParticipant(team, 'Team')\" class=\"btn btn-primary\"> Add To Battle </button>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n            <div v-show=\"contestants\" class=\"row col-lg-6\">\n                <h4> Contestants ({{ contestant_total }})</h4>\n                <table class=\"table table-striped table-bordered\">\n                    <thead>\n                    <tr>\n                        <th>\n                            Name\n                        </th>\n                        <th>\n                            Add to Battle\n                        </th>\n                    </tr>\n                    </thead>\n                    <tbody>\n                    <tr v-for=\"contestant in contestants\">\n                        <td>\n                            {{contestant.name}}\n                        </td>\n                        <td>\n                            <button v-show=\"nb_participants < 5\" @click=\"addParticipant(contestant, 'Contestant')\" class=\"btn btn-primary\"> Add To Battle </button>\n                        </td>\n                    </tr>\n                    </tbody>\n                </table>\n                <div class=\"text-center\" v-show=\"contestant_total\">\n                    <nav>\n                        <ul class=\"pager\">\n                            <li v-bind:class=\"{ 'disabled': contestant_page == 1}\" v-on:click=\"search_contestants(contestant_page - 1)\" class=\"btn\"><a>Previous</a></li>\n                            <li>\n                                <strong> {{ contestant_page }}  / {{ contestant_pages }}</strong>\n                            </li>\n                            <li v-bind:class=\"{ 'disabled': contestant_page == contestant_pages}\" @click=\"search_contestants(contestant_page + 1)\" class=\"btn\"><a>Next</a></li>\n                        </ul>\n                    </nav>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-lg-4 text-center\" >\n            <table v-show=\"nb_participants > 0\" class=\"table table-striped table-bordered\">\n                <caption> Participants to the battle ({{nb_participants}}) </caption>\n                <thead>\n                    <tr>\n                        <th>\n                            Name\n                        </th>\n                        <th>\n                            Type\n                        </th>\n                        <th>\n                            Remove\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr v-for=\"participant in participants\">\n                        <td>\n                            {{participant.name}}\n                        </td>\n                        <td>\n                            {{participant.type}}\n                        </td>\n                        <td>\n                            <button @click=\"deleteParticipant(participant)\" class=\"btn btn-danger\"> Remove From Battle </button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n            <button v-show=\"nb_participants > 0\" class=\"btn btn-success\" @click=\"exists\"> Submit Battle </button>\n            <h3 v-else> No participants for the new Battle </h3>\n        </div>\n    </div>\n</div>\n";

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(56)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/battles/index.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(60)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/battles/index.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var Search = __webpack_require__(57);

	exports.default = {
	    components: {
	        Search: Search
	    }
	};

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(58)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/battles/search.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(59)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/battles/search.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 58 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['list', 'total', 'searchTerm', 'page', 'pages', 'show', 'subject'],

	    created: function created() {
	        this.searchTerm = '';
	        this.page = 1;
	        this.pages = 1;
	        this.searching = false;
	        this.initialSearch(1);
	    },
	    methods: {
	        search: function search(page) {
	            if (this.searchTerm.replace(/\s+/g, '').length >= 3) {
	                this.searching = true;
	                this.$http.post('/api/search/' + this.subject, { term: this.searchTerm, page: page, api_token: window.apiToken }).then(function (data) {
	                    var received_data = data.data;
	                    this.list = received_data.data;
	                    this.total = received_data.total;
	                    this.page = page;
	                    this.pages = received_data.pages;
	                }, function (error) {
	                    console.log(error);
	                });
	            } else {
	                this.searching = false;
	                this.initialSearch(page);
	            }
	        },

	        initialSearch: function initialSearch(page) {
	            this.$http.get('/api/' + this.subject + '/page/' + page, { api_token: window.apiToken }).then(function (data) {
	                var received_data = data.data;
	                this.list = received_data.data;
	                this.total = received_data.total;
	                if (this.total <= 10) {
	                    this.page = 1;
	                } else {
	                    this.page = page;
	                }
	                this.pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        },
	        vote: function vote(id) {
	            this.$http.post('/api/vote/' + this.subject, { id: id, api_token: window.apiToken }).then(function (data) {
	                alertify.success("Vote Recorded");
	                this.initialSearch(1);
	            }, function (error) {
	                console.log(error);
	            });
	        }
	    },
	    events: {
	        'search': function search(term) {
	            this.searchTerm = term;this.search(1);
	        }
	    }
	};

/***/ },
/* 59 */
/***/ function(module, exports) {

	module.exports = "\n    <div class=\"container-fluid\">\n        <form class=\"form-horizontal\" v-show=\"show == 'true'\">\n            <div class=\"form-group text-center\">\n                <label for=\"inputBattle\">Search</label>\n                <div>\n                    <input type=\"text\" @keyup=\"search(1)\" v-model=\"searchTerm\" class=\"form-control\" id=\"inputBattle\" placeholder=\"Battle\">\n                </div>\n            </div>\n        </form>\n        <table class=\"table table-striped table-bordered text-center table-responsive\" v-show=\"total\">\n            <caption> Battles ({{total}}) </caption>\n            <thead class=\"text-center\">\n            <tr>\n                <th>\n                    Number\n                </th>\n                <th>\n                    Name\n                </th>\n\t\t\t\t<th>\n\t\t\t\t\tRound\n\t\t\t\t</th>\n                <th>\n                    Number of votes\n                </th>\n                <th>\n                    Vote\n                </th>\n            </tr>\n            </thead>\n            <tbody>\n            <tr v-for=\"item in list\" :item=\"item\" :index=\"$index\">\n                <td>\n                    {{ ($index + 1)  + ((page - 1) * 10) }}\n                </td>\n                <td style=\"width: 50%\">\n\t\t\t\t\t<ul class=\"list-unstyled\">\n                        <li v-for=\"c in item.contestants\" :c=\"c\" v-link=\"{ name: 'showContestant', params: {id: c.id} }\">\n                         <a>{{ c.name }}</a>\n                        </li>\n                        <li  v-for=\"t in item.teams\" :t=\"t\" v-link=\"{ name: 'showTeam', params: {id: t.id} }\">\n                            <a>{{ t.name }}</a>\n                        </li>\n                    </ul>\n                </td>\n\t\t\t\t<td> {{item.round}} </td>\n                <td>\n                    {{item.nb_votes}}\n                </td>\n                <td>\n                    <button @click=\"vote(item.id)\"  class=\"btn btn-primary\" v-show=\"item.can_vote\">\n                        Vote for this Battle\n                    </button>\n                    <span v-else>\n                        You are not allowed to vote\n                    </span>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n        <h3 v-else> No result found! </h3>\n        <div class=\"text-center\" v-show=\"total\">\n            <nav>\n                <ul class=\"pager\">\n                    <li v-bind:class=\"{ 'disabled': page == 1}\" v-on:click=\"search(page - 1)\" class=\"btn\"><a>Previous</a></li>\n                    <li>\n                        <strong> {{ page }}  / {{ pages }}</strong>\n                    </li>\n                    <li v-bind:class=\"{ 'disabled': page == pages}\" @click=\"search(page + 1)\" class=\"btn\"><a>Next</a></li>\n                </ul>\n            </nav>\n        </div>\n    </div>\n";

/***/ },
/* 60 */
/***/ function(module, exports) {

	module.exports = "\n<search show=\"true\" subject=\"battles\"></search>\n";

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(62)
	__vue_script__ = __webpack_require__(64)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/battles/show.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(65)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/battles/show.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(63);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 64 */
/***/ function(module, exports) {

	"use strict";

/***/ },
/* 65 */
/***/ function(module, exports) {

	module.exports = "\n\n";

/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(67)
	__vue_template__ = __webpack_require__(69)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/teams/home.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(68);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./home.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 69 */
/***/ function(module, exports) {

	module.exports = "\n<div>\n    <header>\n        <ol class=\"breadcrumb\">\n            <li v-link=\"{ path:  '/teams' , append: true}\" class=\"btn\">The List</li>\n            <li v-link=\"{ path: '/teams/new', append: true }\"  class=\"btn\">Create a new  Team</li>\n        </ol>\n    </header>\n    <div class=\"container-fluid\">\n        <router-view\n                keep-alive>\n        </router-view>\n    </div>\n</div>\n";

/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(71)
	__vue_script__ = __webpack_require__(73)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/teams/new.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(74)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/teams/new.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(72);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./new.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n", ""]);

	// exports


/***/ },
/* 73 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['memberSearch', 'members', 'contestants', 'team_total', 'teamName', 'team_page', 'team_pages', 'contestant_total', 'contestant_page', 'contestant_pages'],
	    components: {},
	    created: function created() {
	        this.memberSearch = '';
	        this.teamName = "";
	        this.page = 1;
	        this.pages = 1;
	        this.search_contestants(1);
	        this.members = new Array();
	    },

	    computed: {
	        nb_members: function nb_members() {
	            return this.members.length;
	        }
	    },
	    methods: {
	        search: function search() {
	            this.search_contestants(1);
	        },
	        addMember: function addMember(member) {
	            var current_member = member;
	            if (this.nb_members >= 5) {
	                alertify.error("The number of members is limited to five!");
	                return true;
	            }
	            if (this.nb_members > 0) {
	                var isNotCurrentMember = function isNotCurrentMember(element, index, array) {
	                    return current_member.id != element.id;
	                };

	                if (this.members.every(isNotCurrentMember)) {
	                    this.members.push(current_member);
	                } else {
	                    alertify.notify("Contestant already selected for team");
	                }
	            } else {
	                this.members.push(member);
	            }
	        },
	        deleteMember: function deleteMember(member) {
	            this.members.$remove(member);
	        },
	        search_contestants: function search_contestants(page) {
	            this.$http.post('/api/search/contestants', { term: this.memberSearch, page: page }).then(function (data) {
	                var received_data = data.data;
	                this.contestants = received_data.data;
	                this.contestant_total = received_data.total;
	                this.contestant_page = page;
	                this.contestant_pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        },
	        similarTeams: function similarTeams() {
	            if (this.teamName.length >= 3) {
	                this.$http.post('/api/teams/similar', {
	                    api_token: window.apiToken,
	                    name: this.teamName
	                }).then(function (data) {
	                    var received_data = data.data;
	                    console.log(received_data);
	                }, function (error) {
	                    alertify.error("An error occured while looking for similar team names");
	                });
	            }
	        },
	        create_team: function create_team() {
	            if (this.teamName.length < 3) {
	                alertify.error("You need to specify a name for the Team");
	                return;
	            }

	            this.$http.post('/api/teams/create', { api_token: window.apiToken,
	                team: { members: this.members, name: this.teamName } }).then(function (data) {
	                console.log(data);
	                if (data.data.msg) {
	                    alertify.success(data.data.msg);
	                    this.reset();
	                }
	            }, function (error) {
	                console.log(error);
	                if (error.data.msg) {
	                    alertify.error(error.data.msg);
	                } else {
	                    alertify.error(error.data.errors);
	                }
	                this.reset();
	            });
	        },
	        reset: function reset() {
	            this.memberSearch = "";
	            this.teamName = "";
	            this.search();
	            this.members = new Array();
	        }
	    }
	};

/***/ },
/* 74 */
/***/ function(module, exports) {

	module.exports = "\n<div class=\"text-center\">\n    <h3> Create a New Team (Max 5 members) </h3>\n    <div class=\"container-fluid\" >\n        <div class=\"col-lg-8\">\n            <form class=\"form-horizontal\" @submit=\"search\">\n                <div class=\"form-group text-center\">\n                    <label for=\"inputMemberSearch\">Search</label>\n                    <div>\n                        <input type=\"text\" @keyup=\"search\" v-model=\"memberSearch\" class=\"form-control\" id=\"inputMemberSearch\" placeholder=\"Search for potential members\">\n                    </div>\n                </div>\n            </form>\n            <div v-show=\"contestants\" class=\"row col-lg-6\">\n                <h4> Contestants ({{ contestant_total }})</h4>\n                <table class=\"table table-striped table-bordered\">\n                    <thead>\n                    <tr>\n                        <th>\n                            Name\n                        </th>\n                        <th>\n                            Add to Team\n                        </th>\n                    </tr>\n                    </thead>\n                    <tbody>\n                    <tr v-for=\"contestant in contestants\">\n                        <td>\n                            {{contestant.name}}\n                        </td>\n                        <td>\n                            <button v-show=\"nb_members < 5\" @click=\"addMember(contestant, 'Contestant')\" class=\"btn btn-primary\"> Add To Team </button>\n                        </td>\n                    </tr>\n                    </tbody>\n                </table>\n                <div class=\"text-center\" v-show=\"contestant_total\">\n                    <nav>\n                        <ul class=\"pager\">\n                            <li v-bind:class=\"{ 'disabled': contestant_page == 1}\" v-on:click=\"search_contestants(contestant_page - 1)\" class=\"btn\"><a>Previous</a></li>\n                            <li>\n                                <strong> {{ contestant_page }}  / {{ contestant_pages }}</strong>\n                            </li>\n                            <li v-bind:class=\"{ 'disabled': contestant_page == contestant_pages}\" @click=\"search_contestants(contestant_page + 1)\" class=\"btn\"><a>Next</a></li>\n                        </ul>\n                    </nav>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-lg-4 text-center\" >\n            <div class=\"form-group text-center\">\n                    <label for=\"inputTeamName\">Search</label>\n                    <div>\n                        <input type=\"text\" @click=\"similarTeams()\"  v-model=\"teamName\" class=\"form-control\" id=\"inputTeamName\" placeholder=\"The name of the Team\">\n                    </div>\n                </div>\n            <table v-show=\"nb_members > 0\" class=\"table table-striped table-bordered\">\n                <caption> Members of the team  ({{nb_members}}) </caption>\n                <thead>\n                    <tr>\n                        <th>\n                            Name\n                        </th>\n                        \n                        <th>\n                            Remove\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr v-for=\"member in members\">\n                        <td>\n                            {{member.name}}\n                        </td>\n                       \n                        <td>\n                            <button @click=\"deleteMember(member)\" class=\"btn btn-danger\"> Remove From Team </button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n            <button v-show=\"nb_members > 0\" class=\"btn btn-success\" @click=\"create_team\"> Submit Team </button>\n            <h3 v-else> No Members for the new team! </h3>\n        </div>\n    </div>\n</div>\n";

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(76)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/teams/index.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(80)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/teams/index.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var Search = __webpack_require__(77);

	exports.default = {
	    components: {
	        Search: Search
	    }
	};

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__vue_script__ = __webpack_require__(78)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/teams/search.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(79)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/teams/search.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 78 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    props: ['list', 'total', 'searchTerm', 'page', 'pages', 'show', 'subject'],

	    created: function created() {
	        this.searchTerm = '';
	        this.page = 1;
	        this.pages = 1;
	        this.searching = false;
	        this.initialSearch(1);
	    },
	    methods: {
	        search: function search(page) {
	            if (this.searchTerm.replace(/\s+/g, '').length >= 3) {
	                this.searching = true;
	                this.$http.post('/api/search/' + this.subject, { term: this.searchTerm, page: page, api_token: window.apiToken }).then(function (data) {
	                    var received_data = data.data;
	                    this.list = received_data.data;
	                    this.total = received_data.total;
	                    this.page = page;
	                    this.pages = received_data.pages;
	                }, function (error) {
	                    console.log(error);
	                });
	            } else {
	                this.searching = false;
	                this.initialSearch(page);
	            }
	        },

	        initialSearch: function initialSearch(page) {
	            this.$http.get('/api/' + this.subject + '/page/' + page, { api_token: window.apiToken }).then(function (data) {
	                var received_data = data.data;
	                this.list = received_data.data;
	                this.total = received_data.total;
	                if (this.total <= 10) {
	                    this.page = 1;
	                } else {
	                    this.page = page;
	                }
	                this.pages = received_data.pages;
	            }, function (error) {
	                console.log(error);
	            });
	        },
	        vote: function vote(id) {
	            this.$http.post('/api/vote/' + this.subject, { id: id, api_token: window.apiToken }).then(function (data) {
	                alertify.success("Vote Recorded");
	                this.initialSearch(1);
	            }, function (error) {
	                console.log(error);
	            });
	        }
	    },
	    events: {
	        'search': function search(term) {
	            this.searchTerm = term;this.search(1);
	        }
	    }
	};

/***/ },
/* 79 */
/***/ function(module, exports) {

	module.exports = "\n    <div class=\"container-fluid\">\n        <form class=\"form-horizontal\" v-show=\"show == 'true'\">\n            <div class=\"form-group text-center\">\n                <label for=\"inputBattle\">Search</label>\n                <div>\n                    <input type=\"text\" @keyup=\"search(1)\" v-model=\"searchTerm\" class=\"form-control\" id=\"inputTeam\" placeholder=\"Team or Contestant\">\n                </div>\n            </div>\n        </form>\n        <table class=\"table table-striped table-bordered text-center table-responsive\" v-show=\"total\">\n            <caption> Teams ({{total}}) </caption>\n            <thead class=\"text-center\">\n            <tr>\n                <th>\n                    Number\n                </th>\n                <th>\n                    Name\n                </th>\n\t\t\t\t<th>\n          Contestants\n\t\t\t\t</th>\n                <th>\n                    Number of votes\n                </th>\n                <th>\n                    Vote\n                </th>\n            </tr>\n            </thead>\n            <tbody>\n            <tr v-for=\"item in list\" :item=\"item\" :index=\"$index\">\n                <td>\n                    {{ ($index + 1)  + ((page - 1) * 10) }}\n                </td>\n                <td>\n                  {{item.name}}\n                </td>\n                <td style=\"width: 50%\">\n\t\t\t\t\t<ul class=\"list-unstyled\">\n                        <li v-for=\"c in item.contestants\" :c=\"c\" v-link=\"{ name: 'showContestant', params: {id: c.id} }\">\n                         <a>{{ c.name }}</a>\n                        </li>\n\n          </ul>\n                </td>\n                <td>\n                    {{item.nb_votes}}\n                </td>\n                <td>\n                    <button @click=\"vote(item.id)\"  class=\"btn btn-primary\" v-show=\"item.can_vote\">\n                        Vote for this Team\n                    </button>\n                    <span v-else>\n                        You are not allowed to vote\n                    </span>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n        <h3 v-else> No result found! </h3>\n        <div class=\"text-center\" v-show=\"total\">\n            <nav>\n                <ul class=\"pager\">\n                    <li v-bind:class=\"{ 'disabled': page == 1}\" v-on:click=\"search(page - 1)\" class=\"btn\"><a>Previous</a></li>\n                    <li>\n                        <strong> {{ page }}  / {{ pages }}</strong>\n                    </li>\n                    <li v-bind:class=\"{ 'disabled': page == pages}\" @click=\"search(page + 1)\" class=\"btn\"><a>Next</a></li>\n                </ul>\n            </nav>\n        </div>\n    </div>\n";

/***/ },
/* 80 */
/***/ function(module, exports) {

	module.exports = "\n<search show=\"true\" subject=\"teams\"></search>\n";

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_script__, __vue_template__
	__webpack_require__(82)
	__vue_script__ = __webpack_require__(84)
	if (__vue_script__ &&
	    __vue_script__.__esModule &&
	    Object.keys(__vue_script__).length > 1) {
	  console.warn("[vue-loader] web/static/vue/components/teams/show.vue: named exports in *.vue files are ignored.")}
	__vue_template__ = __webpack_require__(85)
	module.exports = __vue_script__ || {}
	if (module.exports.__esModule) module.exports = module.exports.default
	if (__vue_template__) {
	(typeof module.exports === "function" ? (module.exports.options || (module.exports.options = {})) : module.exports).template = __vue_template__
	}
	if (false) {(function () {  module.hot.accept()
	  var hotAPI = require("vue-hot-reload-api")
	  hotAPI.install(require("vue"), true)
	  if (!hotAPI.compatible) return
	  var id = "/Users/mackbook/Prog/elixir/erb_voting/apps/webapp/web/static/vue/components/teams/show.vue"
	  if (!module.hot.data) {
	    hotAPI.createRecord(id, module.exports)
	  } else {
	    hotAPI.update(id, module.exports, __vue_template__)
	  }
	})()}

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(83);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(7)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/vue-loader/lib/style-rewriter.js!./../../../../../node_modules/vue-loader/lib/selector.js?type=style&index=0!./show.vue");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(6)();
	// imports


	// module
	exports.push([module.id, "\n\n", ""]);

	// exports


/***/ },
/* 84 */
/***/ function(module, exports) {

	"use strict";

/***/ },
/* 85 */
/***/ function(module, exports) {

	module.exports = "\n\n";

/***/ }
/******/ ]);