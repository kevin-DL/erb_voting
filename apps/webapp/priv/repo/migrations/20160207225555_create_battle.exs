defmodule Webapp.Repo.Migrations.CreateBattle do
  use Ecto.Migration

  def change do
    create table(:battles) do
      add :status, :integer
      add :round, :integer
      add :link, :string
      add :name, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps
    end
    create index(:battles, [:user_id])
    create unique_index(:battles, [:name])
  end
end
