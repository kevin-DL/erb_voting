defmodule Webapp.Repo.Migrations.CreateMember do
  use Ecto.Migration

  def change do
    create table(:members) do
      add :contestant_id, references(:contestants, on_delete: :nothing)
      add :team_id, references(:teams, on_delete: :nothing)

      timestamps
    end
    create index(:members, [:contestant_id])
    create index(:members, [:team_id])
    create unique_index(:members, [:contestant_id, :team_id])

  end
end
