defmodule Webapp.Repo.Migrations.CreateParticipant do
  use Ecto.Migration

  def change do
    create table(:participants) do
      add :battle_id, references(:battles, on_delete: :nothing)
      add :contestant_id, references(:contestants, on_delete: :nothing)
      add :team_id, references(:teams, on_delete: :nothing)

      timestamps
    end
    create index(:participants, [:battle_id])
    create index(:participants, [:contestant_id])
    create index(:participants, [:team_id])

    create unique_index(:participants, [:battle_id, :contestant_id])
    create unique_index(:participants, [:battle_id, :team_id])

  end
end
