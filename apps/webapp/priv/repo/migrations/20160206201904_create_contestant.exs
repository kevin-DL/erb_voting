defmodule Webapp.Repo.Migrations.CreateContestant do
  use Ecto.Migration

  def change do
    create table(:contestants) do
      add :name, :string
      add :normalized_name, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps
    end
    create index(:contestants, [:user_id])
    create unique_index(:contestants, [:normalized_name])

  end
end
