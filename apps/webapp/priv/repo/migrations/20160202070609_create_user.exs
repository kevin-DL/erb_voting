defmodule Webapp.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :nickname, :string
      add :provider, :string
      add :status, :integer, default: 1
      add :image, :string
      add :uid, :string
      add :admin, :boolean, default: false

      timestamps
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:provider, :uid])

  end
end
