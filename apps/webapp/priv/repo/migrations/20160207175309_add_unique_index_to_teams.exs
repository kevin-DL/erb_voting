defmodule Webapp.Repo.Migrations.AddUniqueIndexToTeams do
  use Ecto.Migration

  def change do
    create unique_index(:teams, [:normalized_name])
  end
end
