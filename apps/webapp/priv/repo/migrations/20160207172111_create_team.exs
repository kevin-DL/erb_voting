defmodule Webapp.Repo.Migrations.CreateTeam do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :name, :string
      add :normalized_name, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps
    end
    create index(:teams, [:user_id])

  end
end
