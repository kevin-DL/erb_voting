defmodule Webapp.Repo.Migrations.FixBattleUniqueIndex do
  use Ecto.Migration

  def change do
  	drop index(:battles, [:name])
  	create unique_index(:battles, [:name, :round])
  end
end
