defmodule Webapp.Repo.Migrations.CreateVote do
  use Ecto.Migration

  def change do
    create table(:votes) do
      add :user_id, references(:users, on_delete: :nothing)
      add :battle_id, references(:battles, on_delete: :nothing)
      add :contestant_id, references(:contestants, on_delete: :nothing)
      add :team_id, references(:teams, on_delete: :nothing)

      timestamps
    end
    create index(:votes, [:user_id])
    create index(:votes, [:battle_id])
    create index(:votes, [:contestant_id])
    create index(:votes, [:team_id])
    create unique_index(:votes, [:user_id, :battle_id])
    create unique_index(:votes, [:user_id, :team_id])
    create unique_index(:votes, [:user_id, :contestant_id])

  end
end
