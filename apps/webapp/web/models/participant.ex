defmodule Webapp.Participant do
  use Webapp.Web, :model

  schema "participants" do
    belongs_to :battle, Webapp.Battle
    belongs_to :contestant, Webapp.Contestant
    belongs_to :team, Webapp.Team

    timestamps
  end

  @required_fields_contestants ~w(battle_id contestant_id)
  @required_fields_teams ~w(battle_id team_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def contestant_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields_contestants, @optional_fields)
    |> unique_constraint(:contestant_id, name: :participants_battle_id_contestant_id_index)    
    |> assoc_constraint(:contestant)
    |> assoc_constraint(:battle)
  end

  def team_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields_teams, @optional_fields)
    |> unique_constraint(:team_id, name: :participants_battle_id_team_id_index)
    |> assoc_constraint(:team)    
    |> assoc_constraint(:battle)
  end
end
