defmodule Webapp.Vote do
  use Webapp.Web, :model

  schema "votes" do
    belongs_to :user, Webapp.User
    belongs_to :battle, Webapp.Battle
    belongs_to :contestant, Webapp.Contestant
    belongs_to :team, Webapp.Team

    timestamps
  end

  @required_fields_battle ~w(user_id battle_id)
  @required_fields_contestant ~w(user_id contestant_id)
  @required_fields_team ~w(user_id team_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def battle_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields_battle, @optional_fields)
    |> assoc_constraint(:battle)
    |> assoc_constraint(:user)
  end

  def contestant_changeset(model, params \\ :empty) do
      model
      |> cast(params, @required_fields_contestant, @optional_fields)
      |> assoc_constraint(:battle)
      |> assoc_constraint(:contestant)
    end

  def team_changeset(model, params \\ :empty) do
      model
      |> cast(params, @required_fields_team, @optional_fields)
      |> assoc_constraint(:battle)
      |> assoc_constraint(:team)
  end
end
