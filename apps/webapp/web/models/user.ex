defmodule Webapp.User do
  use Webapp.Web, :model

  schema "users" do
    field :email, :string
    field :nickname, :string
    field :provider, :string
    field :status, :integer, default: 1
    field :image, :string
    field :admin, :boolean, default: false
    field :uid, :string
    field :name, :string

    has_many :contestants, Webapp.Contestant
    has_many :teams, Webapp.Team
    has_many :battles, Webapp.Battle
    has_many :votes, Webapp.Vote

    timestamps
  end

  @required_fields ~w(email provider uid)
  @optional_fields ~w(nickname image status admin)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:uid, name: :users_provider_uid_index)
    |> unique_constraint(:email)
    |> set_name()
  end


  def update_changeset(model, params \\ :empty) do
    model
    |> cast(params, [], ~w(name nickname))
    |> validate_length(:name, min: 1, max: 20)
    |> validate_length(:nickname, min: 6, max: 20)
  end


  @doc """
  Sets the name of the user, if it is defined, otherwise it is the email adress
  """
  defp set_name(changeset) do
    email = get_change(changeset, :email)
    name = get_change(changeset, :name)
    if is_nil(name) do
      put_change(changeset, :name, email)
    end
  end
end
