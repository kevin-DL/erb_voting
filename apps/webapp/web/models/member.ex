defmodule Webapp.Member do
  use Webapp.Web, :model

  schema "members" do
    belongs_to :contestant, Webapp.Contestant
    belongs_to :team, Webapp.Team

    timestamps
  end

  @required_fields ~w(contestant_id team_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> assoc_constraint(:contestant)
    |> assoc_constraint(:team)
  end
end
