defmodule Webapp.Battle do
  use Webapp.Web, :model

  schema "battles" do
    field :status, :integer, default: 1
    field :round, :integer, default: 1
    field :link, :string
    field :name, :string
    belongs_to :user, Webapp.User
    has_many :participants, Webapp.Participant
    has_many :contestants, through: [:participants, :contestant]
    has_many :teams, through: [:participants, :team]
    has_many :votes, Webapp.Vote
    has_many :voters, through: [:votes, :user]

    timestamps
  end

  @required_fields ~w(status name user_id)
  @optional_fields ~w(round)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:name)
    |> validate_length(:name, min: 3)
    |> validate_number(:round, greater_than_or_equal_to: 1)
    |> assoc_constraint(:user)
    |> unique_constraint(:name, name: :battles_name_round_index)
  end
end
