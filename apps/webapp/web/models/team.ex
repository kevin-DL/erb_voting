defmodule Webapp.Team do
  use Webapp.Web, :model

  schema "teams" do
    field :name, :string
    field :normalized_name, :string
    belongs_to :user, Webapp.User
    has_many :members, Webapp.Member
    has_many :contestants, through: [:members, :contestant]
    has_many :participants, Webapp.Participant
    has_many :battles, through: [:participants, :battle]
    has_many :votes, Webapp.Vote
    has_many :voters, through: [:votes, :user]

    timestamps
  end

  @required_fields ~w(name user_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> normalize_name()
    |> unique_constraint(:normalized_name)
    |> assoc_constraint(:user)
  end

  defp normalize_name(changeset) do
      if name = get_change(changeset, :name) do
          normalized_name =
              Regex.replace(~r/[^\w]/iu, name, "")
              |> String.downcase
          put_change(changeset, :normalized_name, normalized_name)
      else
          changeset
      end
    end
end
