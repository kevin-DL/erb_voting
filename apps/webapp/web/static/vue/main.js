// main.js
//services

// require a *.vue component
var Home = require("./components/home.vue")
var Login = require("./components/login.vue")
var About =  require("./components/about.vue")

// Contestants
var ContestantHome = require("./components/contestants/home.vue")
var ContestantNew = require("./components/contestants/new.vue")
var ContestantIndex = require("./components/contestants/index.vue")
var ContestantShow = require("./components/contestants/show.vue")

// Battles
var BattleHome = require("./components/battles/home.vue")
var BattleNew = require("./components/battles/new.vue")
var BattleIndex = require("./components/battles/index.vue")
var BattleShow = require("./components/battles/show.vue")

// Teams
var TeamHome = require("./components/teams/home.vue")
var TeamNew = require("./components/teams/new.vue")
var TeamIndex = require("./components/teams/index.vue")
var TeamShow = require("./components/teams/show.vue")



// The router needs a root component to render.
// For demo purposes, we will just use an empty one
// because we are using the HTML as the app template.
// !! Note that the App is not a Vue instance.

var App = Vue.extend({
})

// Create a router instance.
// You can pass in additional options here, but let's
// keep it simple for now.
var router = new VueRouter({
    history: false,
    linkActiveClass: 'disabled'
})

// Define some routes.
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
router.map({
    '/': {
      component: Home
    },
    '/login': {
        component: Login
    },
    '/about': {
        component: About
    },
    '/contestants':{
        component: ContestantHome,

        subRoutes:{
            '/': {
                component: ContestantIndex,
            },
            '/new':{
                component: ContestantNew
            },
            '/:id':{
                name: 'showContestant',
                component: ContestantShow
            }

        }
    },
    '/battles':{
        component: BattleHome,

        subRoutes:{
            '/': {
                component: BattleIndex
            },
            '/new':{
                component: BattleNew
            },
            '/:id':{
                name: 'showBattle',
                component: BattleShow
            }

        }
    },
    '/teams':{
        component: TeamHome,
        subRoutes:{
            '/':{
                component: TeamIndex
            },
            '/new':{
                component: TeamNew
            },
            '/:id':{
                name: 'showTeam',
                component: TeamShow
            }
        }
    }
})

router.beforeEach(function () {
    window.scrollTo(0, 0)
})

// Now we can start the app!
// The router will create an instance of App and mount to
// the element matching the selector #app.
router.start(App, '#app')
