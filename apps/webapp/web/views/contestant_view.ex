defmodule Webapp.ContestantView do
  use Webapp.Web, :view

  def render("index.json", %{contestants: contestants, total: total}) do
    %{data: render_many(contestants, Webapp.ContestantView, "contestant.json"),
    total: total,
    pages: calculate_pages(total)
    }
  end

  def render("show.json", %{contestant: contestant}) do
    %{data: render_one(contestant, Webapp.ContestantView, "contestant.json")}
  end

  def render("contestant.json", %{contestant: cont}) do
    contestant = cont.c
    %{id: contestant.id,
      name: contestant.name,
      normalized_name: contestant.normalized_name,
      user_id: contestant.user_id,
      owner: contestant.user.name,
      nb_teams: cont.nb_teams,
      nb_battles: cont.nb_battles,
      nb_votes: cont.nb_votes}
  end

  defp calculate_pages(total, offset \\ 10) do
    cond do
        total <= 10 -> 1
        true -> case rem(total, offset) do
            0 -> div(total, offset)
            _ -> div(total, offset) + 1
        end
    end
  end
end
