defmodule Webapp.BattleView do
  use Webapp.Web, :view

  def render("index.json", %{battles: battles, total: total}) do
      %{data: render_many(battles, Webapp.BattleView, "battle.json"),
      total: total,
      pages: calculate_pages(total)
      }
    end

  def render("show.json", %{battle: battle}) do
    %{
      id: battle.id,
      name: battle.name,
      round: battle.round
    }
  end

  def render("battle.json", %{battle: bat}) do
    battle = bat.b
    contestants = cond do
      battle.contestants == nil -> []
      true -> render_many(battle.contestants, Webapp.BattleView, "contestant.json", as: :contestant)
    end

    teams = cond do
      battle.teams == nil -> []
      true -> render_many(battle.teams, Webapp.BattleView, "team.json")
    end
    %{
      id: battle.id,
      status: battle.status,
      user_id: battle.user_id,
      round: battle.round,
      link: battle.link,
      name: battle.name,
      teams: teams,
      contestants: contestants,
      nb_votes: bat.nb_votes,
      can_vote: bat.can_vote
    }
  end

  def render("battle_exists.json", %{battle: battle}) do
    %{
      exists: true,
      name: battle.name,
      round: battle.round
    }
  end

  def render("battle_not_exists.json", %{battle: battle}) do
    %{
      exists: false
    }
  end

  def render("contestant.json", %{contestant: contestant}) do
    %{
      id: contestant.id,
      name: contestant.name,
      normalized_name: contestant.normalized_name
    }
  end

  def render("team.json", %{team: team}) do
    %{}
  end

  defp calculate_pages(total, offset \\ 10) do
      cond do
          total <= 10 -> 1
          true -> case rem(total, offset) do
              0 -> div(total, offset)
              _ -> div(total, offset) + 1
          end
      end
    end
end
