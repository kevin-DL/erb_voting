defmodule Webapp.ParticipantView do
  use Webapp.Web, :view

  def render("index.json", %{participants: participants}) do
    %{data: render_many(participants, Webapp.ParticipantView, "participant.json")}
  end

  def render("show.json", %{participant: participant}) do
    %{data: render_one(participant, Webapp.ParticipantView, "participant.json")}
  end

  def render("participant.json", %{participant: participant}) do
    %{id: participant.id,
      battle_id: participant.battle_id,
      contestant_id: participant.contestant_id,
      team_id: participant.team_id}
  end
end
