defmodule Webapp.SearchView do
  use Webapp.Web, :view
  
  def render("contestants.json", %{contestants: contestants, total: total}) do
      %{data: render_many(contestants, Webapp.ContestantView, "contestant.json"),
        total: total,
        pages: calculate_pages(total)
       }
    end
  
    def render("show_contestant.json", %{contestant: contestant}) do
      %{data: render_one(contestant, Webapp.ContestantView, "contestant.json")}
    end
  
    def render("contestant.json", %{contestant: contestant}) do
      %{id: contestant.id,
        name: contestant.name,
        normalized_name: contestant.normalized_name,
        user_id: contestant.user_id,
        owner: contestant.user.name}
    end

    def render("battles.json", %{battles: battles, total: total}) do
      %{data: render_many(battles, Webapp.BattleView, "battle.json"),
      total: total,
      pages: calculate_pages(total)
      }
    end
    def render("teams.json", %{teams: teams, total: total})  do
      %{
        data: render_many(teams, Webapp.TeamView, "team.json"),
        total: total,
        pages: calculate_pages(total)
      } 
    end
   """ 
    def render("team.json") do
    
    end
  
    def render("battle.json") do
    
    end
    """



    defp calculate_pages(total, offset \\ 10) do
        cond do
            total <= 10 -> 1
           true -> case rem(total, offset) do
                0 -> div(total, offset)
                _ -> div(total, offset) + 1
            end
        end
      end
end
