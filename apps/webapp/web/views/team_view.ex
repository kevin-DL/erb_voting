defmodule Webapp.TeamView do
  use Webapp.Web, :view

  def render("index.json", %{teams: teams, total: total}) do
    %{
      data: render_many(teams, Webapp.TeamView, "team.json"),
      total: total,
      pages: calculate_pages(total)
     }
  end

  def render("show.json", %{team: team}) do
    %{data: render_one(team, Webapp.TeamView, "team.json")}
  end

  def render("team.json", %{team: unformatted_team}) do
    team = unformatted_team.t
    contestants = cond do
      team.contestants == nil -> []
      true -> render_many(team.contestants, Webapp.BattleView, "contestant.json", as: :contestant)
    end

    %{
      id: team.id,
      name: team.name,
      normalized_name: team.normalized_name,
      user_id: team.user_id,
      contestants: contestants,
      nb_votes: unformatted_team.nb_votes,
      can_vote: unformatted_team.can_vote
    }
  end

  defp calculate_pages(total, offset \\ 10) do
    cond do
      total <= 10 -> 1
      true ->
        case rem(total, offset) do
          0 -> div(total, offset)
          _ -> div(total, offset) + 1
        end
    end
  end
end
