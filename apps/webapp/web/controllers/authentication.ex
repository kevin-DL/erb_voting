defmodule Webapp.Authentication do
  import Plug.Conn
  import Phoenix.Controller
  alias Webapp.Router.Helpers

  @max_age  2 * 7 * 24 * 3600

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user_id = get_session(conn, :user_id)

    cond do
      user = conn.assigns[:current_user] -> put_current_user(conn, user)
      user = user_id && repo.get(Webapp.User, user_id) -> put_current_user(conn,user)
      true -> assign(conn, :current_user, nil)
    end
  end

  def authenticate_user(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
       conn
       |> put_flash(:error, "You are not logged in!")
       |> redirect(to: Helpers.page_path(conn, :index))
       |> halt()     
    end
  end

  def is_admin(conn, _opts) do
     if conn.assigns.current_user.admin do
       conn
     else
       conn
       |> put_flash(:error, "This zone is exclusivly for Administrators")
       |> redirect(to: Helpers.page_path(conn, :index))
       |> halt()
     end
  end


  def authenticate_api_user(conn, _opts) do
    %{"api_token" => api_token} = conn.params
    case Phoenix.Token.verify(conn, "api", api_token, max_age: @max_age) do
        {:ok, user_id} ->
            conn
            |> assign(:user_id, user_id)
        {:error, _reason} ->
            conn
            |> put_status(:forbidden)
            |> render(Webapp.ChangesetView, "failure.json", error: "You need to be logged in to perform this action!")
            |> halt
    end
  end

  defp put_current_user(conn, user) do
    token = Phoenix.Token.sign(conn, "user socket", user.id)
    api_token = Phoenix.Token.sign(conn, "api", user.id)

    conn
    |> assign(:current_user, user)
    |> assign(:user_token, token)
    |> assign(:api_token, api_token)
  end

end
