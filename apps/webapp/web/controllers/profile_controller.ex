defmodule Webapp.ProfileController do
  use Webapp.Web, :controller
  alias Webapp.User

  def index(conn, _params) do
    user_id = get_session(conn, :user_id)
    user = Repo.get(Webapp.User, user_id)
    changeset = User.changeset(user)

    render conn, "index.html", user: user, changeset: changeset
  end

  def update(conn, %{"user" => user_params}) do
    user_id = get_session(conn, :user_id)
    user = Repo.get(Webapp.User, user_id)
    changeset = User.update_changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Profile updated successfully.")
        |> redirect(to: profile_path(conn, :index))
      {:error, changeset} ->
        render(conn, "index.html", user: user, changeset: changeset)
    end


  end
end
