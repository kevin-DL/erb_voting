defmodule Webapp.TeamController do
  use Webapp.Web, :controller

  alias Webapp.Team
  alias Webapp.Vote
  alias Webapp.Contestant
  alias Webapp.Battle
  alias Webapp.Member

  import Webapp.Normalize
  import Ecto.Query

  plug :scrub_params, "team" when action in [:create, :update]

  def index(conn, _params) do
    teams = Repo.all(Team)
    render(conn, "index.json", teams: teams)
  end


  def page(conn, %{"page" => page, "api_token" => api_token}) do
    computed_offset = cond do
      page <= 0 || page == nil -> 0
      true -> (String.to_integer(page) - 1) * 10
    end

    query = cond do
      api_token != "" -> connection = conn |> authenticate_api_user([])
      from(
        t in Team,
        left_join: v in Vote,
        on: v.id == v.team_id,
        limit: 10,
        offset: ^computed_offset,
        preload: [:user, :contestants, :battles],
        group_by: t.id,
        select: %{t: t, nb_votes: count(v.id), can_vote: count(v.user_id == ^connection.assigns.user_id) == 0},
        order_by: [desc: count(v.id), asc: :name]
      )
      true ->
          from(
            t in Team,
            left_join: v in Vote,
            on: v.id == v.battle_id,
            limit: 10,
            offset: ^computed_offset,
            group_by: t.id,
            select: %{t: t, nb_votes: count(v.id), can_vote: false},
            order_by: [desc: count(v.id), asc: :name]
          ) 
    end

    teams = Repo.all(query)
    total_query =
      Team |> select([t], count(t.id))

    total = Repo.one!(total_query)
    render(conn, "index.json", teams: teams, total: total)
  end

  def top(conn, _params) do
    query =
      from(
        t in Team,
        left_join: v in Vote,
        on: t.id == v.team_id,
        limit: 10,
        offset: 0, 
        preload: [:user, :contestants],
        group_by: t.id,
        select: %{t: t, nb_votes: count(v.id), can_vote: false},
        order_by: [desc: count(v.id), asc: :name]
      )

    teams = Repo.all(query)

    total_query = Team |> select([t], count(t.id))
    total = Repo.one!(total_query)

    render(conn, "index.json", teams: teams, total: total)
  end

  def create(conn, %{"team" => team_params}) do
    team_parameters = Map.put_new(team_params, "user_id", conn.assigns.user_id)
    changeset = Team.changeset(%Team{}, team_parameters)
    %{"members" => members} = team_parameters

    case Repo.insert(changeset) do
      {:ok, team} ->

        Enum.each(members, fn(%{"id" => contestant_id} = member) ->
          member_changeset = Member.changeset(%Member{}, %{team_id: team.id, contestant_id: contestant_id})
          Repo.insert(member_changeset)
          end
        )

        conn
        |> put_status(:created)
        |> put_resp_header("location", team_path(conn, :show, team))
        |> render(Webapp.ChangesetView, "success.json", message: "Team Created!")
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
    end
  end


  def similar_teams(conn, %{"name" => term}= params) do
    normalized_term = "%" <> normalize_string(term) <> "%"

    query =
      from( t in Team,
            where: like(t.normalized_name, ^normalized_term),
            preload: [:user, :contestants],
            group_by: t.id,
            select: %{t: t, nb_votes: 0, can_vote: false},
            order_by: [asc: :name]
      )

    teams = Repo.all(query)
     
    render(conn, "index.json", teams: teams, total: 10)
  end

end
