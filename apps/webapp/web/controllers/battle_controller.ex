defmodule Webapp.BattleController do
  use Webapp.Web, :controller

  alias Webapp.Battle
  alias Webapp.Participant
  alias Webapp.Contestant
  alias Webapp.Team

  alias Webapp.Vote

  import Ecto.Query
  import Webapp.Normalize

  plug :scrub_params, "battle" when action in [:create, :update]

  def index(conn, _params) do
    battles = Repo.all(Battle)
    render(conn, "index.json", battles: battles)
  end


  def page(conn, %{"page" => page, "api_token" => api_token}) do
    computed_offset = cond do
      page <= 0 ->  0
      true -> (String.to_integer(page) - 1) * 10
    end
    query = cond do
      api_token != "" ->  connection = conn |> authenticate_api_user([])
      from( b in Battle,
        left_join: v in Vote, on: b.id == v.battle_id,
        limit: 10,
        offset: ^computed_offset,
        preload: [:user, :contestants, :teams],
        group_by: b.id,
        select: %{b: b, nb_votes: count(v.id), can_vote: count(v.user_id == ^connection.assigns.user_id) == 0},
        order_by: [desc: count(v.id), asc: :name])
      true            -> from( b in Battle,
        left_join: v in Vote, on: b.id == v.battle_id,
        limit: 10,
        offset: ^computed_offset,
        preload: [:user, :contestants, :teams],
        group_by: b.id,
        select: %{b: b, nb_votes: count(v.id), can_vote: false},
        order_by: [desc: count(v.id), asc: :name])
    end
    battles = Repo.all(query)

    total_query =
    Battle
    |> select([b], count(b.id))

    total = Repo.one!(total_query)
    render(conn, "index.json", battles: battles, total: total)
  end

  def top(conn, _params) do
    query =
    from( b in Battle,
      left_join: v in Vote, on: b.id == v.battle_id,
      limit: 10,
      offset: 0,
      preload: [:user, :contestants, :teams],
      group_by: b.id,
      select: %{b: b, nb_votes: count(v.id), can_vote: false},
      order_by: [desc: count(v.id), asc: :name])
    battles = Repo.all(query)

    total_query =
    Battle
    |> select([b], count(b.id))
    total = Repo.one!(total_query)
    render(conn, "index.json", battles: battles, total: total)
  end



  def create(conn, %{"battle" => battle_params}) do
    battl_params = Map.put_new(battle_params, "user_id", conn.assigns.user_id)

    %{
      contestants: contestants,
      participants: participants,
      teams: teams,
      contestants_exist: contestants_exist,
      teams_exist: teams_exist,
      battle_name: battle_name,
      battle_parameters: battle_parameters
      } = process_parameters(conn, battl_params)
      current_round = get_current_round(battle_name)
      battle_parameters = cond do
        current_round ==  nil ->  battle_parameters
        true ->  Map.put_new(battle_parameters, "round", current_round.round + 1)
      end

      changeset = Battle.changeset(%Battle{}, battle_parameters)


      Repo.transaction(fn ->
        case Repo.insert(changeset) do
          {:ok, battle} ->
#Contestants
Enum.each(contestants, fn(%{"id" => contestant_id} = contestant) ->
  participant_changeset = Participant.contestant_changeset(%Participant{}, %{battle_id: battle.id, contestant_id: contestant_id})
  Repo.insert(participant_changeset)
end
)
#Teams
Enum.each(teams, fn(%{"id" => team_id} = team) ->
  participant_changeset = Participant.team_changeset(%Participant{}, %{battle_id: battle.id, team_id: team_id})
  Repo.insert(participant_changeset)
end
)
conn
|> put_status(:created)
|> put_resp_header("location", battle_path(conn, :show, battle))
|> render(Webapp.ChangesetView, "success.json", message: "Battle created: " <> battle_name)
{:error, changeset} ->
  conn
  |> put_status(:unprocessable_entity)
  |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
end
end)
    end

    def show(conn, %{"id" => id}) do
      battle = Repo.get!(Battle, id)
      render(conn, "show.json", battle: battle)
    end

    def delete(conn, %{"id" => id}) do
      battle = Repo.get!(Battle, id)

# Here we use delete! (with a bang) because we expect
# it to always work (and if it does not, it will raise).
Repo.delete!(battle)

send_resp(conn, :no_content, "")
end

def exists(conn, %{"battle" => battle_params}) do
  battl_params = Map.put_new(battle_params, "user_id", conn.assigns.user_id)

  %{
    contestants: contestants,
    participants: participants,
    teams: teams,
    contestants_exist: contestants_exist,
    teams_exist: teams_exist,
    battle_name: battle_name,
    battle_parameters: battle_parameters
    } = process_parameters(conn, battl_params)

    current_round = get_current_round(battle_name)
    case current_round do
      nil -> conn
              |> render(Webapp.BattleView, "battle_not_exists.json", battle: nil)
      _   -> conn
              |> render(Webapp.BattleView, "battle_exists.json", battle: current_round)
    end


  end

  defp process_parameters(conn, battl_params) do
    %{"participants" => participants} =  battl_params
    contestants = Enum.filter(participants, fn(%{"type" => type} = x) -> type == "Contestant" end)
    teams = Enum.filter(participants, fn(%{"type" => type} = x) -> type == "Team" end)

    contestants_exist = Enum.all?(contestants, fn(%{"id" => id} = x) ->
      Repo.get(Webapp.Contestant, id) !== nil
    end
    )

    teams_exist = Enum.all?(teams, fn(%{"id" => id} = x) ->
      Repo.get(Webapp.Team, id) !== nil
    end
    )

    cond do
      Enum.count(participants) == 0 ->  conn
      |> put_status(:unprocessable_entity)
      |> render(Webapp.ChangesetView, "failure.json", error: "No participants were specified for this battle")
      Enum.count(contestants) == 0  && Enum.count(teams) == 0 ->  conn
      |> put_status(:unprocessable_entity)
      |> render(Webapp.ChangesetView, "failure.json", error: "The specified participants don't have the right type")
      !contestants_exist && !teams_exist -> conn
      |> put_status(:unprocessable_entity)
      |> render(Webapp.ChangesetView, "failure.json", error: "The specified participants don't have the right type")
      Enum.count(teams) + Enum.count(contestants) > 5 -> conn |> put_status(:unprocessable_entity) 
      |> render(Webapp.ChangesetView, "failure.json", error: "The number of participants is limited to 5")
      true -> IO.puts("Has participants")
    end
    battle_name = generate_name(participants)
    battle_parameters = Map.put_new(battl_params, "name", battle_name)

    %{
      participants: participants,
      contestants: contestants,
      teams: teams,
      contestants_exist: contestants_exist,
      teams_exist: teams_exist,
      battle_name: battle_name,
      battle_parameters: battle_parameters
    }
  end

  defp generate_name(participants) do

    verified_participants =
    Enum.reduce(participants, [], fn(%{"id" => id, "type" => type} =x, acc) ->
      participant = case type do
        "Contestant" -> Repo.get!(Contestant, id)
        "Team" -> Repo.get!(Team, id)
      end
      [participant | acc]
    end
    )


    sorted_participants =
    Enum.sort(verified_participants, fn(p1, p2) ->
      p1.name < p2.name
    end
    )

    battle_name = Enum.map_join(sorted_participants, " vs ", fn(x) -> x.name end)
    battle_name
  end

  defp get_current_round(battle_name) do
    query = from(b in Battle,
      where: b.name == ^battle_name,
      order_by: [desc: b.round],
      limit: 1,
      select: b)

    last_battle = Repo.one(query)
  end
end
