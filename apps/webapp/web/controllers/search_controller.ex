defmodule Webapp.SearchController do
    use Webapp.Web, :controller

    alias Webapp.Contestant
    alias Webapp.Battle
    alias Webapp.Team
    alias Webapp.Vote
    alias Webapp.Member
    alias Webapp.Participant
    import Ecto.Query
    import Webapp.Normalize

  @moduledoc false


  def all(conn, %{"term" => term}) do

  end

  def contestants(conn, %{"term" => term, "page" => page} = params) do
    normalized_term = "%" <> normalize_string(term) <> "%"
    computed_offset = cond do
        page <= 0 ->  0
        true -> (page - 1) * 10
    end

     query =
            from( c in Contestant,
            left_join: m in Member, on: c.id == m.contestant_id,
            left_join: t in Team, on: t.id == m.team_id,
            left_join: p in Participant, on: c.id == p.contestant_id,
            left_join: b in Battle, on: b.id == p.battle_id,
            left_join: v in Vote, on: c.id == v.contestant_id,
             where: like(c.normalized_name, ^normalized_term),
             limit: 10,
             offset: ^computed_offset,
             preload: [:user],
             group_by: c.id,
             select: %{c: c, nb_teams: count(t.id), nb_battles: count(b.id), nb_votes: count(v.id)},
             order_by: [desc: count(v.id), asc: :name])
        contestants = Repo.all(query)


        total_query =
            from( c in Contestant,
                where: like(c.normalized_name, ^normalized_term),
                select: count(c.id))
            total = Repo.one!(total_query)




    render(conn, "contestants.json", contestants: contestants, total: total)
  end

  def teams(conn, %{"term" => term, "page" => page, "api_token" => api_token} = params) do
    normalized_term = "%" <> normalize_string(term) <> "%"
      fixed_term = "%" <> term <> "%"
    computed_offset = cond do
        page <= 0 ->  0
        true -> (page - 1) * 10
    end

    query = cond do
      api_token != "" -> connection = conn |> authenticate_api_user([])
        from( t in Team,
              left_join: m in Member,
              on: t.id == m.team_id,
              left_join: c in Contestant,
              on:  c.id == m.contestant_id,
              left_join: v in Vote, on: t.id == v.team_id,
              where: like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
              limit: 10,
              offset: ^computed_offset,
              preload: [:user, :contestants],
              group_by: t.id,
              select: %{t: t, nb_votes: count(v.id), can_vote: count(v.user_id == ^connection.assigns.user_id) == 0},
              order_by: [desc: count(v.id), asc: :name])
      true ->
          from(
            t in Team,
            left_join: m in Member,
            on: t.id == m.team_id,
            left_join: c in Contestant,
            on:  c.id == m.contestant_id,
            left_join: v in Vote,
            on: t.id == v.team_id,
            where: like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
            limit: 10,
            offset: ^computed_offset,
            preload: [:user, :contestants],
            group_by: t.id,
            select: %{t: t, nb_votes: count(v.id), can_vote: false},
         order_by: [desc: count(v.id), asc: :name])
    end
    teams = Repo.all(query)

    query_total = from( t in Team,
    left_join: m in Member, on: t.id == m.team_id,
    left_join: c in Contestant, on:  c.id == m.contestant_id,
    left_join: v in Vote, on: t.id == v.team_id,
    where: like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
    select: count(t.id, :distinct))

    total = Repo.one!(query_total)

    render(conn, "teams.json", teams: teams, total: total)
  end

  def battles(conn, %{"term" => term, "page" => page, "api_token" => api_token} = params) do
      normalized_term = "%" <> normalize_string(term) <> "%"
      fixed_term = "%" <> term <> "%"
    computed_offset = cond do
        page <= 0 ->  0
        true -> (page - 1) * 10
    end

    query = cond do
      api_token != "" -> connection = conn |> authenticate_api_user([])
        from( b in Battle,
        left_join: p in Participant, on: b.id == p.battle_id,
        left_join: c in Contestant, on:  c.id == p.contestant_id,
        left_join: t in Team, on: t.id == p.team_id,
        left_join: v in Vote, on: b.id == v.battle_id,
        where: ilike(b.name, ^fixed_term) or like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
         limit: 10,
         offset: ^computed_offset,
         preload: [:user, :contestants, :teams],
         group_by: b.id,
         select: %{b: b, nb_votes: count(v.id), can_vote: count(v.user_id == ^connection.assigns.user_id) == 0},
         order_by: [desc: count(v.id), asc: :name])
      true ->
        from( b in Battle,
        left_join: p in Participant, on: b.id == p.battle_id,
        left_join: c in Contestant, on:  c.id == p.contestant_id,
        left_join: t in Team, on: t.id == p.team_id,
        left_join: v in Vote, on: b.id == v.battle_id,
        where: ilike(b.name, ^fixed_term) or like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
         limit: 10,
         offset: ^computed_offset,
         preload: [:user, :contestants, :teams],
         group_by: b.id,
         select: %{b: b, nb_votes: count(v.id), can_vote: false},
         order_by: [desc: count(v.id), asc: :name])
    end
    battles = Repo.all(query)

    query_total = from( b in Battle,
    left_join: p in Participant, on: b.id == p.battle_id,
    left_join: c in Contestant, on:  c.id == p.contestant_id,
    left_join: t in Team, on: t.id == p.team_id,
    left_join: v in Vote, on: b.id == v.battle_id,
    where: ilike(b.name, ^fixed_term) or like(c.normalized_name, ^normalized_term) or like(t.normalized_name, ^normalized_term),
    select: count(b.id, :distinct))

    total = Repo.one!(query_total)

    render(conn, "battles.json", battles: battles, total: total)
  end


end
