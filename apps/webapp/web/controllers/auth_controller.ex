defmodule Webapp.AuthController do
  use Webapp.Web, :controller
  alias Webapp.Repo
  alias Webapp.User
  plug Ueberauth

  alias Ueberauth.Strategy.Helpers

  def request(conn, _params) do
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out!")
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

  def callback(%{ assigns: %{ ueberauth_failure: fails } } = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def callback(%{ assigns: %{ ueberauth_auth: auth } } = conn, params) do
    find_user(conn, auth)
  end

  defp find_user(conn, auth) do
    user = Repo.get_by(Webapp.User, [uid: auth.uid, provider: to_string(auth.provider)])
    cond do
        user ->
            conn
            |> put_flash(:info, "Successfully authenticated.")
            |> assign(:current_user, user)
            |> put_session(:user_id, user.id)
            |> redirect(to: "/")
        true -> create_user(conn, auth)
    end
  end

  defp create_user(conn, user_params) do
    changeset = User.changeset(%User{}, modify_info(user_params))

    case Repo.insert(changeset) do
        {:ok, user} ->
            conn
            |> put_flash(:info, "Successfully authenticated")
            |> assign(:current_user, user)
            |> put_session(:user_id, user.id)
            |> redirect(to: "/")
        {:error, reason} ->
            conn
            |> put_flash(:error, reason)
            |> redirect(to: "/")
    end
  end

  def modify_info(auth) do
   %{
      email: auth.info.email,
      provider: to_string(auth.provider),
      uid: auth.uid,
      name: auth.info.name,
      nickname: auth.info.nickname,
      image: auth.info.image
    }
  end
end
