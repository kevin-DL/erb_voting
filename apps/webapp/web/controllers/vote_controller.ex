defmodule Webapp.VoteController do
  use Webapp.Web, :controller

  alias Webapp.Vote
  alias Webapp.Battle
  alias Webapp.Repo
  alias Webapp.Team
  
  plug :scrub_params, "vote" when action in [:create, :update]

  def battle(conn, %{"id" => id}) do
    battle = Repo.get(Battle, id)
    if battle == nil do
     conn |> render(Webapp.ChangesetView, "failure.json", error: "This battle does not exis
     t" )
    end

    if has_voted(:battle, battle, conn.assigns.user_id) != nil do
      conn |> render(Webapp.ChangesetView, "failure.json", error: "You have already voted for this battle")
    end

    changeset = Vote.battle_changeset(%Vote{}, %{user_id: conn.assigns.user_id, battle_id: battle.id})

    case Repo.insert(changeset) do
      {:ok, vote} -> conn |> put_status(:created) |> render(Webapp.ChangesetView, "success.json", message: "Your vote has been taken into account")
      {:error, changeset} -> conn |> put_status(:unprocessable_entity) |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def team(conn, %{"id" => id}) do
    team = Repo.get(Team, id)
    case team do
      nil -> conn |> render(Webapp.ChangesetView, "failure.json", error: "This team does not exist")
      _  -> if has_voted(:team, team, conn.assigns.user_id) do
               conn |> render(Webapp.ChangesetView, "failure.json", error: "You have already voted for this Team")
            end
    end

    changeset = Vote.team_changeset(%Vote{}, %{user_id: conn.assigns.user_id, team_id: team.id})

    case Repo.insert(changeset) do
      {:ok, vote} -> conn |> put_status(:created) |> render(Webapp.ChangesetView, "success.json", message: "Your vote has been taken into account")
      {:error, changeset} -> conn |> put_status(:unprocessable_entity) |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def contestant do

  end


  defp has_voted(type, target, user_id) do
    base_query = from(
      v in Vote,
      where: v.user_id == ^user_id
    )
    element_query = case type do
                      :battle -> from(v in base_query, where: v.battle_id == ^target.id)
                      :team -> from(v in base_query, where: v.team_id == ^target.id)
                    end
    query = from( v in element_query, select: v)

    vote = Repo.one(query)
    vote
  end
end
