defmodule Webapp.ContestantController do
  use Webapp.Web, :controller

    alias Webapp.Contestant
      alias Webapp.Battle
      alias Webapp.Team
      alias Webapp.Vote
      alias Webapp.Member
      alias Webapp.Participant
      import Ecto.Query
      import Webapp.Normalize

  plug :scrub_params, "contestant" when action in [:create, :update]
  plug :authenticate_api_user, "contestant" when action in [:create, :update]

  def index(conn, _params) do
    query =
        Contestant
        |> order_by(asc: :name)
        |> limit(10)
        |> offset(0)
        |> preload([:user])
    contestants = Repo.all(query)

    total_query =
        Contestant
        |> select([c], count(c.id))
    total = Repo.one!(total_query)
    render(conn, "index.json", contestants: contestants, total: total)
  end

  def page(conn, %{"page" => page}) do
    computed_offset = cond do
        page <= 0 ->  0
        true -> (String.to_integer(page) - 1) * 10
    end
    query =
        from( c in Contestant,
                    left_join: m in Member, on: c.id == m.contestant_id,
                    left_join: t in Team, on: t.id == m.team_id,
                    left_join: p in Participant, on: c.id == p.contestant_id,
                    left_join: b in Battle, on: b.id == p.battle_id,
                    left_join: v in Vote, on: c.id == v.contestant_id,
                     limit: 10,
                     offset: ^computed_offset,
                     preload: [:user],
                     group_by: c.id,
                     select: %{c: c, nb_teams: count(t.id), nb_battles: count(b.id), nb_votes: count(v.id)},
                     order_by: [desc: count(v.id), asc: :name])
    contestants = Repo.all(query)    

    total_query =
            Contestant
            |> select([c], count(c.id))
        total = Repo.one!(total_query)
        render(conn, "index.json", contestants: contestants, total: total)
  end

  def top(conn, _params) do
      query =
             from( c in Contestant,
                         left_join: m in Member, on: c.id == m.contestant_id,
                         left_join: t in Team, on: t.id == m.team_id,
                         left_join: p in Participant, on: c.id == p.contestant_id,
                         left_join: b in Battle, on: b.id == p.battle_id,
                         left_join: v in Vote, on: c.id == v.contestant_id,
                          limit: 10,
                          offset: 0,
                          preload: [:user],
                          group_by: c.id,
                          select: %{c: c, nb_teams: count(t.id), nb_battles: count(b.id), nb_votes: count(v.id)},
                          order_by: [desc: count(v.id), asc: :name])
         contestants = Repo.all(query)

         total_query =
                 Contestant
                 |> select([c], count(c.id))
             total = Repo.one!(total_query)
             render(conn, "index.json", contestants: contestants, total: total)
    end

  def create(conn, %{"contestant" => contestant}) do
    contestant_params = Map.put_new(contestant, "user_id", conn.assigns.user_id)
    changeset = Contestant.changeset(%Contestant{}, contestant_params)
    case Repo.insert(changeset) do
      {:ok, contestant} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", contestant_path(conn, :show, contestant))
        |> render(Webapp.ChangesetView, "success.json", message: "Contestant created")
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contestant_query =
        Contestant
        |> where([id: ^id])
        |> preload([:user])

    contestant = Repo.one!(contestant_query)
    render(conn, "show.json", contestant: contestant)
  end

  def update(conn, %{"id" => id, "contestant" => contestant_params}) do
    contestant = Repo.get!(Contestant, id)
    changeset = Contestant.changeset(contestant, contestant_params)

    case Repo.update(changeset) do
      {:ok, contestant} ->
        render(conn, "show.json", contestant: contestant)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Webapp.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contestant = Repo.get!(Contestant, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(contestant)

    send_resp(conn, :no_content, "")
  end
end
