defmodule Webapp.Router do
  use Webapp.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Webapp.Authentication, repo: Webapp.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Webapp do
    pipe_through :browser # Use the default browser stack

    get     "/about", PageController, :about
    get     "/login", PageController, :login
    get     "/", PageController, :index
  end

  scope "/profile", Webapp do
    pipe_through [:browser, :authenticate_user]

    patch   "/", ProfileController, :update
    put     "/", ProfileController, :update
    get     "/", ProfileController, :index
  end

  #Authentication scope
  scope "/auth", Webapp do
    pipe_through :browser

    get     "/:provider", AuthController, :request
    get     "/:provider/callback", AuthController, :callback
    post    "/:provider/callback", AuthController, :callback
    delete  "/logout", AuthController, :delete
  end

  scope "/admin", Webapp do
    pipe_through [:browser, :authenticate_user, :is_admin]


  end

  #Other scopes may use custom stacks.
  scope "/api", Webapp do
    pipe_through [:api]
    #Public API
    scope "/top" do
      get "/contestants", ContestantController, :top
      get "/teams/", TeamController, :top
      get "/battles/", BattleController, :top
    end

    scope "/bottom" do
      get "/contestants/", ContestantController, :botom
      get "/teams/", TeamController, :bottom
      get "/battles/", BattleController, :bottom
    end

    scope "/contestants" do
      get     "/page/:page", ContestantController, :page
      get     "/:id", ContestantController, :show
      get     "/", ContestantController, :index
    end

    scope "/battles" do
      get     "/page/:page", BattleController, :page
      get     "/:id", BattleController, :show
      get     "/", BattleController, :index
    end

    scope "/teams" do
      get     "/page/:page", TeamController, :page
      get     "/:id", TeamController, :show
      get     "/", TeamController, :index
    end

    scope "/search" do
      post "/contestants", SearchController, :contestants
      post "/teams", SearchController, :teams
      post "/battles", SearchController, :battles
      post "/", SearchController, :all
    end

    #Private Api
    pipe_through [:authenticate_api_user]

    scope "/contestants" do
      post    "/create", ContestantController, :create

      patch   "/:id", ContestantController, [:update, :authenticate_api_user]

      put     "/:id", ContestantController, [:update, :authenticate_api_user]

      #delete  "/:id", ContestantController, [:delete, :authenticate_api_user]
    end

    scope "/battles" do
      post    "/create", BattleController, :create
      patch   "/:id", BattleController, :update
      put     "/:id", BattleController, :update
      post "/exists", BattleController, :exists

      #delete  "/:id", ContestantController, [:delete, :authenticate_api_user]
    end

    scope "/vote" do
       post "/battles", VoteController, :battle
       post "/contestants", VoteController, :contestant
       post "/teams", VoteController, :team      
    end

    scope "/teams" do
      post "/create", TeamController, :create
      post "/similar", TeamController, :similar_teams
    end
    resources "/participants", ParticipantController, except: [:new, :edit]
    resources "/votes", VoteController, except: [:new, :edit]
  end
end
