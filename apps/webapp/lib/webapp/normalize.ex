defmodule Webapp.Normalize do

  def normalize_string(str) do
    Regex.replace(~r/[^\w]/iu, str, "")
    |> String.downcase
  end
  
end